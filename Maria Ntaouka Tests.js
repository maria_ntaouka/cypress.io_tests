/// <reference types="Cypress" />

context('Actions', () => {
	Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})
  beforeEach(() => {
    cy.visit('https://www.apothema.gr')
  })

  it('1. Test signup process (Create new account / signup)', () => {
	cy.get('[data-target="#register1"]').should('be.visible').click()
	  
    cy.get('#form-mail')
      .type('c1880919@urhen.com').should('have.value', 'c1880919@urhen.com')
	cy.get('#form-reemail')
      .type('c1880919@urhen.com').should('have.value', 'c1880919@urhen.com')
	cy.get('#form-password')
      .type('Aa1234567').should('have.value', 'Aa1234567')
	cy.get('#form-repassword')
      .type('Aa1234567').should('have.value', 'Aa1234567')
	  
    cy.get('[onclick="doRegister();"]').click()
  })

it('3. Try to unsuccessfully login with wrong credentials (this way we test that a user cannot login with a wrong password)', () => {
	
	cy.visit('https://www.apothema.gr')
	
	
	cy.get('[data-target="#register"]').should('be.visible').click({multiple: true,force: true})
	  
    cy.get('#form-mail')
      .type('c1880919@urhen.com').should('have.value', 'c1880919@urhen.com')
	  
	cy.get('#form-password1').clear()
      
	cy.get('#form-password')
      .type('Aa123').should('have.value', 'Aa123')
	  
    cy.get('[onclick="doLogin();"]').click()
	cy.wait(1500)
  })

 it('2. Try to successfully login with correct credentials (this way we test that login works fine)', () => {
	
	cy.visit('https://www.apothema.gr')
	
	cy.get('[data-target="#register"]').should('be.visible').click({multiple: true,force: true})
	  
    cy.get('#form-mail')
      .type('c1880919@urhen.com').should('have.value', 'c1880919@urhen.com')
	  
	cy.get('#form-password1').clear()
      
	cy.get('#form-password')
      .type('Aa1234567').should('have.value', 'Aa1234567')
	  
    cy.get('[onclick="doLogin();"]').click()
  })
  
  it('4. Try to search for the text ψυγεία and check that the page you end up to has 18 products return don first page and that the total pages are more than 20.', () => {
	
	cy.visit('https://www.apothema.gr')
	
	cy.get('#SearchForm').within(() => {
		cy.get('[name="q"]').type('ψυγεία').should('have.value', 'ψυγεία')
		cy.get('[value="L"]').click()
	})
	
	cy.get('[name="pageForm"]').within(() => {
		cy.get('[name="pageSize"]').should('have.value', '18')
	})
	
	cy.get('.pager').find('li').find('a').should('have.length', 20)
	
  })
})
